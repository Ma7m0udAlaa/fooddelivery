//
//  UITableViewCell.swift
//  Tutors
//
//  Created by Mahmoud Alaa on 5/20/19.
//  Copyright © 2019 MEST EG1. All rights reserved.
//

import UIKit

extension UITableViewHeaderFooterView {
    
    class var identifier: String {
        return String(describing: self)
    }
    
}


