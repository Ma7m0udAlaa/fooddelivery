//
//  UITableView+Extensions.swift
//  FoodDelivery
//
//  Created by Osama on 10/24/19.
//  Copyright © 2019 Osama Gamal. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func registerHeaderNib<Cell: UITableViewHeaderFooterView>(cellClass: Cell.Type){
        self.register(UINib(nibName: cellClass.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: cellClass.identifier)
    }
    
    func registerCellNib<Cell: UITableViewCell>(cellClass: Cell.Type){
        self.register(UINib(nibName: cellClass.identifier, bundle: nil), forCellReuseIdentifier: cellClass.identifier)
    }
    
    
    func dequeue<Cell: UITableViewCell>() -> Cell{

        guard let cell = self.dequeueReusableCell(withIdentifier: Cell.identifier) as? Cell else {
            fatalError("Error in cell")
        }
        
        return cell
    }
    
}

