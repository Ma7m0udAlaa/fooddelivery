//
//  UITableViewCell.swift
//  Tutors
//
//  Created by Mahmoud Alaa on 5/20/19.
//  Copyright © 2019 MEST EG1. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    class var identifier: String {
        return String(describing: self)
    }
    
}


