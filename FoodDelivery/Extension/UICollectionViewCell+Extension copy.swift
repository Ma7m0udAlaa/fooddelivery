//
//  CollectionView+Extensions.swift
//  Tutors
//
//  Created by Mahmoud Alaa on 4/29/19.
//  Copyright © 2019 MEST EG1. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    class var identifier: String {
        return String(describing: self)
    }
}
